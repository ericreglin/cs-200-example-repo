#include <iostream>   // Use cin and cout
#include <string>     // Use strings
using namespace std;

//Gitlab comment 
void Program1()
{
    string name;
    string homeTown;

    cout << "Enter your hometown: ";
    cin >> homeTown;

    if (homeTown.size() > 6)
    {
        cout << "That's a long name! \n";
    }
    cout << "Enter your name: \n";
    cin >> name;

    cout << "Hello " << name << " from " << homeTown << "!";

}
// comment
void Program2()
{
    float userPoints = 0;
    float totalPoints = 0;
    float grade = 0;

    cout << "How many points does the assignment have?   \n";
    cin >> totalPoints;
    cout << "How many points did you get?   \n";
    cin >> userPoints;
    grade = (userPoints / totalPoints) * 100;
    cout << "Grade:   " << grade << "\n";

    if (grade >= 60)
    {
        cout << "You Passed!";
    }

    else
    {
        cout << "You failed!";
    }
}

void Program3()
{
    int charge;

    cout << "Enter your phone charge: ";
    cin >> charge;

    if (charge >= 75)
    {
        cout << "[****]";
    }

    else if (charge >= 50)
    {
        cout << "[***_]";
    }

    else if (charge >= 25)
    {
        cout << "[**__]";
    }

    else if (charge >= 5)
    {
        cout << "[*___]";
    }

    else
    {
        cout << "[____]";
    }

}

void Program4()
{
    int userChoice;

    cout << "What is your favorite type of book?\n";
    cout << "1. Scfi\n";
    cout << "2. Historical\n";
    cout << "3. Fantasy\n";
    cout << "4. DIY\n";
    cin >> userChoice;
    cout << "Your selection: " << userChoice << "\n";

    if (userChoice >= 1 && userChoice <= 4)
    {
        cout << "Good choice!";
    }
    
    else
    {
        cout << "Invalid choice";
    }
}

void Program5()
{
    float num1;
    float num2;
    float result;
    char mathOperator;

    cout << "Enter your first number: \n";
    cin >> num1;
    cout << "Enter your second number: \n";
    cin >> num2;
    cout << "What kind of operation?\n";
    cout << "+: add\n";
    cout << "-: subtract\n";
    cout << "*: multiply\n";
    cout << "/: divide\n";
    cin >> mathOperator;

    
    switch (mathOperator)
    {
    case '+' :
        result = num1 + num2;
        
        break;

    case '-' :
        result = num1 - num2;
        
        break;

    case '*' :
        result = num1 * num2;
        ;
        break;

    case '/' :
        if (num1  == 0 || num2 == 0)
        {
            cout << "Can't divide by 0!";
        }
        else
        {
            result = num1 / num2;
            
        }
            break;
    }
    cout << "Result: " << result;
}

// Don't modify main
int main()
{
    while ( true )
    {
        cout << "Run which program? (1-5): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
